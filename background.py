
import numpy as np
import pandas as pd
from matplotlib import rcParams
import matplotlib.pyplot as plt
import astropy.convolution as cv
plt.style.use('seaborn')
style = {
    'figure.figsize': (12, 8),
    'axes.labelsize': 14,
    'axes.titlesize': 16,
    'legend.fontsize': 14,
}
rcParams.update(style)
import os
import pymc3 as pm


def data_input(filename, freq_step=None, num_points=None, high_pass=12, box_width=6):

    full_data = pd.read_csv(filename, sep='\s+', comment='#', names=['freq', 'pow'])

    if high_pass is not None:
        mask = full_data['freq'] > high_pass
    else:
        mask = np.ones(len(full_data))

    if freq_step == num_points:
        raise ValueError('Please provide ONE of freq_step or num_poitns')
    elif freq_step is not None:
        frequencies = np.arange(full_data['freq'][mask].min(), full_data['freq'][mask].max(), freq_step)
    elif num_points is not None:
        frequencies = np.linspace(full_data['freq'][mask].min(), full_data['freq'][mask].max(), num_points)

    data_step = (full_data['freq'][mask][1:].values - full_data['freq'][mask][:-1].values).mean()
    smooth_pow = cv.convolve(full_data['pow'][mask].values, cv.Box1DKernel(box_width / data_step))
    interp_pow = np.interp(frequencies, full_data['freq'][mask], smooth_pow)

    reduced_data = pd.DataFrame()
    reduced_data['freq'] = frequencies
    reduced_data['pow'] = interp_pow

    return full_data, reduced_data

class BackgroundModel:

    num_harveys = 3

    @classmethod
    def response(self, f, nyquist_freq='sc'):
        if nyquist_freq == 'sc':
            nyquist_freq = 8496.355743094671
        elif nyquist_freq == 'lc':
            nyquist_freq = 283.31

        theta = np.pi / 2 * f / nyquist_freq

        try:
            return pm.math.sin(theta)**2 / theta**2
        except:
            return np.sin(theta)**2 / theta**2

    @classmethod
    def harvey(self, f, a, b):
        return 2 * np.sqrt(2) / np.pi * (a**2 / b) / (1 + (f/b)**4)

    @classmethod
    def envelope(self, f, H0, v_max, s):
        try:
            return H0 * pm.math.exp(-(v_max - f)**2 / (2*s**2))
        except:
            return H0 * np.exp(-(v_max - f)**2 / (2*s**2))

    @classmethod
    def background(self, f, W, harvs):
        try:
            return W + self.response(f) * pm.math.sum(harvs, axis=0) 
        except:
            return W + self.response(f) * np.sum(harvs, axis=0) 
        
    @classmethod
    def likelihood(self, f, W, harvs, env):
        return self.background(f, W, harvs) + self.response(f) * env

def inference(data, n_samp=1000, n_tune=2000):
    freq = data['freq'].values
    power = data['pow'].values
    with pm.Model() as background_model:
        k = BackgroundModel.num_harveys
        
        W = pm.Uniform('W', lower=5, upper=20)
        a = pm.Uniform('a', lower=[30, 40, 50], upper=[86, 96, 105], shape=k)
        b = pm.Uniform('b', lower=[1, 80, 300], upper=[10, 210, 500], shape=k)
        
        harvs = [BackgroundModel.harvey(freq, a[i], b[i]) for i in range(k)]
        
        H0 = pm.Uniform('H0', lower=5, upper=30)
        v_max = pm.Uniform('v_max', lower=500, upper=700)
        s = pm.Uniform('s', lower=20, upper=80)
        
        env = BackgroundModel.envelope(freq, H0, v_max, s)
        like = BackgroundModel.likelihood(freq, W, harvs, env)
        y = pm.Exponential('obs', lam=1/like, observed=power)

        trace = pm.sample(n_samp, tune=n_tune)
        ppc = pm.sample_ppc(trace, 1000)
    
    return trace, ppc['obs'].mean(axis=0)

def diagnose(full_data, reduced_data, trace, fit):
    pm.summary(trace)
    plt.figure()
    pm.traceplot(trace)
    plt.tight_layout()
    plt.show()
    plt.figure()
    plt.loglog(full_data['freq'], full_data['pow'], lw=.5, alpha=.6, c='.5', label='Full data')
    plt.loglog(reduced_data['freq'], reduced_data['pow'], 'C0', lw=1, label='Smoothed data')
    plt.loglog(reduced_data['freq'], fit, 'C1', label='Fit')
    a = trace['a'].mean(axis=0)
    b = trace['b'].mean(axis=0)
    harveys = [BackgroundModel.harvey(reduced_data['freq'], a[i], b[i]) for i in range(BackgroundModel.num_harveys)]
    hls = [plt.loglog(reduced_data['freq'], harv, 'C2-.', alpha=.8) for harv in harveys]
    hls[0][0].set_label('Harvey Profile')
    plt.axhline(trace['W'].mean(), c='k', alpha=.3, ls='--', label='White noise')
    plt.ylim(1e-2, None)
    plt.xlim(reduced_data['freq'].min(), reduced_data['freq'].max())
    plt.xlabel(r'Frequency ($\mu$Hz)')
    plt.ylabel('Power')
    plt.tight_layout()
    plt.legend()
    plt.show()

if __name__ == '__main__':
    filename = 'data/kplr007107778_kasoc-psd_slc_v1.pow'
    fdata, rdata = data_input(filename, freq_step=1)
    trace, fit = inference(rdata, n_tune=10000)
    diagnose(fdata, rdata, trace, fit)